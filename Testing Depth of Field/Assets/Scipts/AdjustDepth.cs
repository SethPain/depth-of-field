﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class AdjustDepth : MonoBehaviour {

    public PostProcessingProfile profile;

    [SerializeField]
    private Transform depthOfFieldObject;

    private RaycastHit hit;

    [SerializeField]
    private float maxDistance;

    [SerializeField]
    private LayerMask rayMask;

	// Use this for initialization
	void Start () {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void Update () {

        if (Physics.Raycast(transform.position, transform.forward, out hit, maxDistance, rayMask))
        {
            depthOfFieldObject.position = hit.point;
            
        }
        else depthOfFieldObject.position = transform.position + transform.forward * maxDistance * 0.5f;

        // profile.depthOfField.settings.AdjustFD(Vector3.Distance(depthOfFieldObject.transform.position, transform.position));

        profile.depthOfField.settings.focusDistance = Vector3.Distance(depthOfFieldObject.transform.position, transform.position);

        Debug.Log(Vector3.Distance(depthOfFieldObject.position, transform.position));

        Debug.DrawLine(transform.position, depthOfFieldObject.position, Color.cyan);




        //if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        //{
        //    // Debug.Log(profile.depthOfField.settings.AdjustFD(profile.depthOfField.settings.focusDistance + .5f));
        //    if (profile.depthOfField.settings.focusDistance + 0.1 <= 32)
        //    {
        //        PostProcessingProfile tempProfile = new PostProcessingProfile();
        //        tempProfile.depthOfField.settings.focusDistance = profile.depthOfField.settings.focusDistance + .5f;
        //        profile.depthOfField.settings = tempProfile.depthOfField.settings;
        //    }

        //}
        //else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        //{
        //    // Debug.Log(profile.depthOfField.settings.AdjustFD(profile.depthOfField.settings.focusDistance - .5f));
        //    if (profile.depthOfField.settings.focusDistance - 0.1 >= 0.1)
        //    {
        //        PostProcessingProfile tempProfile = new PostProcessingProfile();
        //        tempProfile.depthOfField.settings.focusDistance = profile.depthOfField.settings.focusDistance - .5f;
        //        profile.depthOfField.settings = tempProfile.depthOfField.settings;
        //    }
        //}



        // Adjust Aperature Ratio Realtime
        if (Input.GetKey(KeyCode.UpArrow))
        {
            Debug.Log(profile.depthOfField.settings.AdjustAperture(profile.depthOfField.settings.aperture + .01f));
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            Debug.Log(profile.depthOfField.settings.AdjustAperture(profile.depthOfField.settings.aperture - .01f));
        }
    }
}
