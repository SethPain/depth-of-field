﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePos : MonoBehaviour {

    public int position = 0;

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (position == 0)
            {
                position = 1;
                gameObject.transform.position = new Vector3(12.5f, 5, -3.5f);
                var rotationVector = transform.rotation.eulerAngles;
                rotationVector.x = 25;
                gameObject.transform.rotation = Quaternion.Euler(rotationVector);
                // gameObject.transform.Rotate(new Vector3(25, 0, 0));
            } else
            {
                position = 0;
                gameObject.transform.position = new Vector3(12.5f, 20, 12.5f);
                var rotationVector = transform.rotation.eulerAngles;
                rotationVector.x = 90;
                gameObject.transform.rotation = Quaternion.Euler(rotationVector);
                // gameObject.transform.Rotate(new Vector3(90, 0, 0));
            }
        }
	}
}
