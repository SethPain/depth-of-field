﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthOfFieldManager : MonoBehaviour {

    [SerializeField]
    private LayerMask rayMask;

    [SerializeField]
    private float maxDistance;

    private RaycastHit hit;

    [SerializeField]
    private Transform depthOfFieldObject;

	
	// Update is called once per frame
	void Update () {
        if (Physics.Raycast(transform.position, transform.forward, out hit, maxDistance, rayMask))
        {
            depthOfFieldObject.position = hit.point;
        }
        else depthOfFieldObject.position = transform.position + transform.forward * maxDistance * 0.5f;
	}
}
